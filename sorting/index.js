const nums = [4, 9, 7, 5, 8, 9, 3]

// INSERTION SORT
function sort(arr) {
  let count = 0
  for (let i = 1; i < arr.length; i++) {
    let marked = arr[i]
    let j = i - 1
    
    while (j >= 0 && marked < arr[j]) {
      arr[j + 1] = arr[j]
      arr[j] = marked
      j--
      count++
      console.log(`${count}. ${arr.join(' ')}`)
    }
  }
  console.log(`Jumlah swap: ${count}`)
  return arr
}

sort(nums);
