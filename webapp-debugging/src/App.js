import React, { useState } from "react";
import "./App.css";
import GitHubLogin from "./GithubLogin";

function App() {
  const [name, setName] = useState("");

  return (
    <div className="App">
      <header className="App-header">
        {name && <h2>Hai {name}</h2>}
        <GitHubLogin
          clientId="2b21da6bfcc53d64a610"
          clientSecret="268cb07fd2b58e4d589d85eb0e642718251a5e2e"
          redirectUri="http://localhost:3000"
          onSuccess={(name) => setName(name.login)}
          onFailure={(resp) => console.log(resp)}
        />
      </header>
    </div>
  );
}

export default App;
